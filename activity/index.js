const http = require("http");

const port = 3000;

const server = http.createServer((request, response)=>{
	if (request.url === "/greeting") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to the login page");
	/*
		Practice - create two more uri's and let your users see the message "Welcome to _____ Page"; use successful status code and plain text as the content
	*/
	}else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("I'm sorry the page you are looking for cannot be found");
	}
});

server.listen(port);

console.log(`Server now running at localhost: ${port}`);
