/*
	miniactivity - load the http module and store it in a variable called http
						 - use 4000 as a value of a variable called port
						 - use server variable as storage of the createServer component
*/
const http = require("http");

const port = 4000;

const server = http.createServer((request, response)=>{
	if (request.url === "/manga") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Manga Page")
	/*
		Practice - create two more uri's and let your users see the message "Welcome to _____ Page"; use successful status code and plain text as the content
	*/
	};

else if (request.url === "/anime") {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.end("Welcome to Anime Page")};




	else{
		response.writeHead(404, {"Content-Type": "text/plain"});
		response.end("Page not found");
	}
});

server.listen(port);

console.log(`Server now running at localhost: ${port}`);
