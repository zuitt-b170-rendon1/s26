let http=require("http")
/*
	require function is a directive/function that is used to load a particular node module; in this case, we are trying to load http module from node.js
		http module - HyperText Transfer Protocol; lets nodejs transfer data; a set of individual files that are needed to create a component (used to establish data transfer between applications)
*/
http.createServer(function(request,response){
/*
	createServer() - allows creation of http server that listens to requests on a specified port and gives response back to the client; accepts a function that allows performing of a task for the server
*/

	response.writeHead(200,{"Content-Type": "text/plain"});
	response.end("Hello World")
}).listen(4000);

console.log("Your server is now running at port: 4000");
/*
	writeHead - used to set a status code for the response; 200 status code is the default code for the node js since this means that the response is successfully processed

	response.end - signals the end of the response process

	listen("4000") - the server will be assigned to the specified port using this command

	port - virtual point where network connections start and end; each port is specific to a certain process/server

	Content-type - sets the type of the content which will be displayed on the client

	ctrl + c -terminate gitbash

*/